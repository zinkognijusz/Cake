# Attraction Layer Cake

 - [Website](https://cake.avris.it)

## Local copy

    make install
    make run
    
## Copyright
 
 * **Author:** Andrea Prusinowski [(Avris.it)](https://avris.it)
 * **Based on:** [a diagram by Luna Rudd](https://imgur.com/gallery/YAGLE)
 * **Licence:** [OQL](https://oql.avris.it/license?c=Andrea%20Prusinowsku%7Chttps://avris.it)
